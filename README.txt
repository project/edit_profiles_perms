This module provides permissions to edit user profiles depending on what
roles the profile requested to be edited has.

INSTRUCTIONS:
Simply enable the module as usual, and go to admin/people/permissions to
configure the permissions under the 'Edit Profiles Permissions' section.
Note that core's 'Administer users' permission still applies,
and will override any permission not set by this module.

USE CASE EXAMPLE:
If you want the 'support' role to be able to edit profiles of users with
the role 'blogger', check 'Edit Blogger users profiles' for the support
users.
